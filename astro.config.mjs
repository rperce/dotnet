import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  site: "https://rperce.net",
  outDir: "public",
  publicDir: "static",
  vite: {
    css: {
      transformer: "lightningcss",
    },
  },
});
